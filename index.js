var express = require('express');
var qs = require('querystring');
var app = express();
var path = require("path");

app.use(express.static('public'));

app.get('/',function(req, res){
  res.sendFile(path.join(__dirname+'/public/43.html'));
});

app.post('/', function (req, res){
    var requestData ='';
    req.setEncoding('utf-8');
 
    req.on('data', function(data) {
      requestData += data;
    });
 
    req.on('end', function() {
      var postData = qs.parse(requestData);
      var data1 = new Date(postData.wiek);
      var data2 = new Date();
      var diff = new Date(data2.getTime() - data1.getTime());
      var wiek = diff.getUTCFullYear() - 1970;
      var fullRes = "<table><th><td>Imie</td><td>Nazwisko</td><td>Wiek</td></th><tr><td></td><td>"+ postData.imie + '</td><td>'+ postData.nazwisko + '</td><td>'+ wiek + '</td></tr></table';
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.end(fullRes);
    });
});

app.all('*', function (req, res) {
   res.status(400).send('Bad request!');
});

var server = app.listen(process.env.PORT || '8080', function () {
 console.log("Serwer slucha na porcie 8080");
});